// Loads express
var express = require("express");

// Loads path
var path = require("path");

// Loads bodyParser
var bodyParser = require("body-parser");

// Loads sequelize ORM
var Sequelize = require("sequelize");

// constants
const NODE_PORT = process.env.PORT || 8080;
const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

// MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 're1nald0';

// instance of express
var app = express(); 

// database
var conn = new Sequelize(
  'grocery',
  MYSQL_USERNAME,
  MYSQL_PASSWORD,
  {
    host: 'localhost',         // default port    : 3306
    logging: console.log,
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    }
  }
);

// Loads models
var GroceryList = require('./models/grocery-list')(conn, Sequelize);

// middlewares
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// routes
app.get("/api/grocerylist", function(req, res){
  GroceryList
    .findAll({})
    .then(function(list){
      res
        .status(200)
        .json(list);
    })
    .catch(function(err){
      res
        .status(500)
        .json(err);
    })
});

// error routes
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT + " " + new Date());
});