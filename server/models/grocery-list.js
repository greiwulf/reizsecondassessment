// model for grocery list
module.exports = function(conn, Sequelize) {
  var GroceryList = conn.define('grocery_list', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    upc12: {
      type: Sequelize.BIGINT(12),
      allowNull: false
    },
    brand: {
      type: Sequelize.STRING,
      allowNull: false    
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false       
    }
  },{
    tableName: 'grocery_list',
    timestamps: false
  });
  return GroceryList;
};