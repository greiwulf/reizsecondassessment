//IIFE
(function(){
  angular
    .module("groceryApp")
    .controller("GrocerylistCtrl", GrocerylistCtrl);

  GrocerylistCtrl.$inject = ["$state", "GroceryListService", "UPC12BarcodeService"];

  function GrocerylistCtrl($state, GroceryListService, UPC12BarcodeService) {
    //view model
    var vm = this;

    //bind
    vm.brand = "";
    vm.name = "";
    vm.listAll = {};
    vm.goEdit = goEdit;
    vm.searchFilter = "";
    //init
    fetchAll();

    function fetchAll(){
      GroceryListService
        .fetchGroceryList()
        .then(function(result){
          // vm.listAll=JSON.stringify(result.data);
          vm.listAll=result.data;
        })
        .catch(function (err) {
          console.log("error: \n" + JSON.stringify(err));
        });
    }

    // function fetchBarcode(upc12){
    //   UPC12BarcodeService
    //     .fetchUpc12Barcode(upc12)
    //     .then(function(result){
    //     vm.listAll=result.data;
    //     })
    //     .catch(function (err) {
    //       console.log("error: \n" + JSON.stringify(err));
    //     });
    // }

     function goEdit(id){
       $state.go("groceryeditlist",{id:id});
     }

  }
  
})();