// client route config
// IIFE
(function(){
  angular
    .module("groceryApp")
    .config(groceryRouteConfig)
    groceryRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function groceryRouteConfig($stateProvider, $urlRouterProvider){
      $stateProvider
        .state('grocerylist', {
                url: '/grocerylist',
                templateUrl: './app/grocerylist/grocerylist.html',
                controller: 'GrocerylistCtrl',
                controllerAs: 'ctrl'
        })
        .state('groceryeditlist', {
                url: '/groceryeditlist',
                templateUrl: './app/groceryeditlist/groceryeditlist.html',
                controller: 'GroceryeditlistCtrl',
                controllerAs: 'ctrl'
        })
      $urlRouterProvider.otherwise("/grocerylist");
    }
})();