(function(){
  angular
    .module("groceryApp")
    .service("UPC12BarcodeService", UPC12BarcodeService);

    //DI
    UPC12BarcodeService.$inject = ['$http'];

    function UPC12BarcodeService($http) {
      var service = this;
      // expose service
      service.fetchUpc12Barcode = fetchUpc12Barcode;

      function fetchUpc12Barcode(upcparams){
        return $http({
          method: 'GET'
          , url: 'http://www.barcodes4.me/barcode/c128a/'
          , params: upcparams + '.png?IsTextDrawn=1'
        });
      }
    }

})();