//IIFE
(function(){
  angular
    .module("groceryApp")
    .service("GroceryListService", GroceryListService);

    //DI
    GroceryListService.$inject = ['$http'];

    function GroceryListService($http) {
      var service = this;
      // expose service
      service.fetchGroceryList = fetchGroceryList;

      function fetchGroceryList(){
        return $http({
          method: 'GET'
          , url: 'api/grocerylist'
        });
      }
    }

})();